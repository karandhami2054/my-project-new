<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form action="form_process.php" enctype="multipart/form-data" class="form" method="post">
					<div class="form-group row">
						<label for="" class="col-sm-3">name</label>
							<div class="col-sm-9">
						<input type="text" name="name" id="name" class="form-control" required="">
							</div>
					</div>


					<div class="form-group row">
						<label for="" class="col-sm-3">address</label>
						<div class="col-sm-9">
							<input type="text" name="address" id="address" class="form-control" required="">
						</div>
					</div>


					<div class="form-group row">
						<label for="" class="col-sm-3">email</label>
						<div class="col-sm-9">
							<input type="email"  name="email" id="email" class="form-control" required="">
						</div>
					</div>

					<div class="form-group row">
						<label for="" class="col-sm-3">password</label>
						<div class="col-sm-9">
							<input type="password"  name="password" id="password" class="form-control" required="">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-3">gender</label>
						<div class="col-sm-9">
							<select name="gender" class="form-control">
								<option value="male">male</option>
								<option value="female">female</option>
								<option value="other">other</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label for="" class="col-sm-3">role</label>
						<div class="col-sm-9">
							<select name="role" class="form-control">
								<option value="reporter">reporter</option>
								<option value="editor">editor</option>
								<option value="admin">admin</option>
							</select>
						</div>
					</div>
                    <div class="form-group row">
                    	<label for="" class="col-sm-3">message</label>
                    	<div class="col-sm-9"><textarea name="message" id="message"  rows="3" class="form-control" required=""></textarea>
                   		 </div>
                    </div>
                    <div class="form-group row">
						<label for="" class="col-sm-3"></label>
						<div class="col-sm-9">
				           <button class="btn btn-success" name="submit">submit</button>
						</div>
					</div>




					
 				</form>
			</div>

		</div>


	</div>

</body>
</html>
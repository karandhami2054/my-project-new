<?php 
require 'db.php' ;


 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<link rel="stylesheet" href="../css/bootstrap.min.css">
 	<title>displaying user data</title>
 </head>
 <body>
 	<div class="container-fluid">
 		<div class="row">
 			<div class="col-md-12">
 				<h1>dispalying user</h1>
 				<table class="table table-hover">
 					<thead>
 						<th>id</th>
 						<th>name</th>
 						<th>address</th>
 						<th>gender</th>
 						<th>role</th>
 						<th>message</th>
 						<th>email</th>
					</thead>
 				    <tbody>
 				    	<?php
 				    	$qry="SELECT * from users ORDER BY id DESC ";
 				    	$run=mysqli_query($con,$qry);
 				    	$countt=mysqli_num_rows($run);
 				    	if($countt<=0){
 				    		echo "no data found int the table";
 				    	}
 				    	else{
 				    		      $count=1;
 				    		while($data=mysqli_fetch_assoc($run)){
 				    			?>
		    				<tr>
 				    			<td> <?php echo $count  ?>  </td>
 				    			<td>   <?php echo $data['name'];  ?> </td>
 				    			<td>   <?php  echo $data['address'];  ?> </td>
								<td>   <?php  echo $data['gender'];  ?> </td>
								<td>   <?php  echo $data['role'];  ?> </td>
								<td>   <?php  echo $data['message'];  ?> </td>
								<td>   <?php  echo $data['email'];  ?> </td>
							</tr>
                          <?php
                                $count++;  
 				    		}
 				    		 
	 		    	}
 				    	?>
 				    </tbody>
 				</div>
 			</div>
 			<a href="form.php">insert</a>
 		</div>
 </body>
 </html>